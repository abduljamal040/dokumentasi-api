<?php

class ModelTask extends CI_Model
{
    public function get_task($id = null)
    {
        if ($id === null) {
            $query = $this->db->get('tasks');
            return $query->result();
        } else {
            $query = $this->db->get_where('tasks', ['category_id' => $id]);
            return $query->result();
        }
    }
    public function delete_task($id)
    {
        $this->db->delete('tasks', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function create_task($data)
    {
        $this->db->insert('tasks', $data);
        return $this->db->affected_rows();
    }

    public function update_task($data, $id)
    {
        $this->db->update('tasks', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}
