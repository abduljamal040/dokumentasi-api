<?php

class ModelTaskCategories extends CI_Model
{
    public function get_task($id = null)
    {
        if ($id === null) {
            $query = $this->db->get('task_categories');
            return $query->result();
        } else {
            $query = $this->db->get_where('task_categories', ['id' => $id]);
            return $query->result();
        }
    }
    public function delete_task($id)
    {
        $this->db->delete('task_categories', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function create_task($data)
    {
        $this->db->insert('task_categories', $data);
        return $this->db->affected_rows();
    }

    public function update_task($data, $id)
    {
        $this->db->update('task_categories', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}
