<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . "libraries/Format.php";
require APPPATH . "libraries/RestController.php";

use chriskacerguis\RestServer\RestController;

class Task extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelTask');
    }
    public function index_get()
    {
        $id = $this->get('category_id');
        if ($id === null) {
            $task = $this->ModelTask->get_task();
        } else {
            $task = $this->ModelTask->get_task($id);
        }
        if ($task) {
            $this->response([
                'status' => true,
                'data' => $task
            ], 200);
        }
        $this->response([
            'status' => false,
            'massage' => 'data not found'
        ], 400);
    }

    public function index_delete()
    {
        $id = $this->delete('id');

        if ($id === null) {
            $this->response([
                'status' => false,
                'massage' => 'error'
            ], 400);
        } else {
            if ($this->ModelTask->delete_task($id) > 0) {
                $this->response([
                    'status' => true,
                    'data' => $id,
                    'massage' => 'deleted'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'massage' => 'id not found'
                ], 400);
            }
        }
    }
    public function index_post()
    {
        $data = [
            'category_id' => $this->post('category_id'),
            'title' => $this->post('title'),
            'description' => $this->post('description'),
            'start_date' => date($this->post('start_date')),
            'finish_date' => date($this->post('finish_date')),
            'status' => $this->post('status')
        ];

        if ($this->ModelTask->create_task($data) > 0) {
            $this->response([
                'status' => true,
                'massage' => 'success'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'massage' => 'failed'
            ], 400);
        }
    }

    public function index_put()
    {
        $id = $this->put('id');
        $data = [
            'category_id' => $this->put('category_id'),
            'title' => $this->put('title'),
            'description' => $this->put('description'),
            'start_date' => date($this->put('start_date')),
            'finish_date' => date($this->put('finish_date')),
            'status' => $this->put('status')
        ];
        if ($this->ModelTask->update_task($data, $id) > 0) {
            $this->response([
                'status' => true,
                'massage' => 'success'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'massage' => 'failed'
            ], 400);
        }
    }
}
