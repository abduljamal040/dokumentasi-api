<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . "libraries/Format.php";
require APPPATH . "libraries/RestController.php";

use chriskacerguis\RestServer\RestController;


class TaskCategories extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelTaskCategories');
    }
    public function index_get()
    {
        $id = $this->get('id');
        if ($id === null) {
            $task = $this->ModelTaskCategories->get_task();
        } else {
            $task = $this->ModelTaskCategories->get_task($id);
        }
        if ($task) {
            $this->response([
                'status' => true,
                'data' => $task
            ], 200);
        }
        $this->response([
            'status' => false,
            'massage' => 'data not found'
        ], 400);
    }

    public function index_delete()
    {
        $id = $this->delete('id');

        if ($id === null) {
            $this->response([
                'status' => false,
                'massage' => 'error'
            ], 400);
        } else {
            if ($this->ModelTaskCategories->delete_task($id) > 0) {
                $this->response([
                    'status' => true,
                    'data' => $id,
                    'massage' => 'deleted'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'massage' => 'id not found'
                ], 400);
            }
        }
    }
    public function index_post()
    {
        $data = [
            'name' => $this->post('name'),
        ];

        if ($this->ModelTaskCategories->create_task($data) > 0) {
            $this->response([
                'status' => true,
                'massage' => 'success'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'massage' => 'failed'
            ], 400);
        }
    }

    public function index_put()
    {
        $id = $this->put('id');
        $data = [
            'name' => $this->put('name'),
        ];
        if ($this->ModelTaskCategories->update_task($data, $id) > 0) {
            $this->response([
                'status' => true,
                'massage' => 'success'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'massage' => 'failed'
            ], 400);
        }
    }
}
